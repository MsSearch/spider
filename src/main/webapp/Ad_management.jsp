<%@ page language="java" contentType="text/html; charset=utf-8"
    pageEncoding="utf-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!doctype html>
<html>
<head>
<meta charset="utf-8">
<title>广告管理</title>
<link rel="stylesheet" type="text/css" href="css/Iframe.css" />
<link rel="stylesheet" href="utilLib/bootstrap.min.css" type="text/css" media="screen" />
<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="stylesheet" href="http://cdn.static.runoob.com/libs/bootstrap/3.3.7/css/bootstrap.min.css">  
	<script src="http://cdn.static.runoob.com/libs/jquery/2.1.1/jquery.min.js"></script>
	<script src="http://cdn.static.runoob.com/libs/bootstrap/3.3.7/js/bootstrap.min.js"></script>
</head>
<script  type="text/javascript">
	function haha()
	{window.location.href="Addadvertising.jsp";}
	function hehe()
	{document.getElementById("submits").submit();}
</script>
<style>
	td,th{padding-left:20px;}
</style>
<body onload="hehe();">
    <div class="table_con">
        <table>
            <tr class="tb_title">
            <td style="margin-top:5px;">
                <button type="button" class="btn btn-info">广告列表</button>
                <button type="button" class="btn btn-info" onClick="haha()">添加广告</button>
            </td></tr>
            <tr><td style="padding-left:40px;"><ul><li>广告列表</li></ul></td></tr>
        </table>
    </div>


    <div class="table_con">
    <form action="advertising.spring" id="submits">
        <table>
         <tr class="tb_title" style="font-weight:bold;">
                <td width="10%">广告ID</td>
                <td width="20%">广告标题</td>
                <td width="22%">链接地址</td>
                <td width="15%">图片地址</td>
                <td width="26%">操作</td>
            </tr>
    
   	<c:forEach items="${adver}" var="advers">
         <tr>
            <td width="10%">${advers.id}</td>
            <td width="20%">${advers.title}</td>
            <td width="22%">${advers.url}</td>
            <td width="15%">${advers.photo_url}</td>
            <td width="26%">
                <input class="sj_btn" type="button" value="修改" />
                <input class="del_btn" type="button" value="删除" />
            </td>
       	</tr>
    </c:forEach>
   </table>
  </form>
 </div>
</body>
</html>


