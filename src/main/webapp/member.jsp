<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!doctype html>
<html>
<head>
<meta charset="utf-8">
<title>会员信息</title>
<link rel="stylesheet" type="text/css" href="css/Iframe.css" />
<link rel="stylesheet" href="utilLib/bootstrap.min.css" type="text/css" media="screen" />
<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="stylesheet" href="http://cdn.static.runoob.com/libs/bootstrap/3.3.7/css/bootstrap.min.css">  
	<script src="http://cdn.static.runoob.com/libs/jquery/2.1.1/jquery.min.js"></script>
	<script src="http://cdn.static.runoob.com/libs/bootstrap/3.3.7/js/bootstrap.min.js"></script>
</head>
<script  type="text/javascript">
	function haha()
	{window.location.href="Addmember.jsp"}
	function hehe()
	{document.getElementById("submits").submit();}
</script>
<style>
	td,th{padding-left:20px;}
</style>
<body onload="hehe();">
	<div class="table_con">
	<table>
    	<tr class="tb_title">
        <td style="margin-top:5px;">
        	<button type="button" class="btn btn-info">会员列表</button>
            <button type="button" class="btn btn-info" onClick="haha()">添加会员</button>
        </td></tr>
        <tr><td style="padding-left:40px;"><ul><li>会员列表</li></ul></td></tr>
    </table>
</div>
    
    <div class="table_con">
    <form action="member.spring" id="submits">
	<table>
     <tr class="tb_title" style="font-weight:bold;">
        	<td width="10%">ID</td>
            <td width="20%">用户名</td>
            <td width="12%">积分</td>
            <td width="20%">邮箱</td>
            <td width="26%">操作</td>
        </tr>
        <c:forEach items="${member}" var="m">
        <tr>
        	<td width="10%">${m.id}</td>
            <td width="20%">${m.name}</td>
            <td width="12%">${m.integral}</td>
            <td width="20%">${m.email}</td>
            <td width="26%">
                <input class="sj_btn" type="button" value="修改" />
                <input class="del_btn" type="button" value="删除" />
            </td>
        </tr>
       </c:forEach> 
             
          <tr>
        	<td colspan="5" style="margin-left:400px;">
                <a>第一页 </a>
                <a>上一页 </a>
                <a>下一页 </a>
                <a>最后一页 </a>
                <a>当前第1页 </a>
                <a>共1页</a>
                <a>共5个记录 </a>
             </td></tr>
    </table>
    </form>
    </div>
    </body>
</html>
