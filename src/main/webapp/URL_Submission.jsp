<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!doctype html>
<html>
<head>
<meta charset="utf-8">
<title>网址提交</title>
<link rel="stylesheet" type="text/css" href="css/Iframe.css" />
<link rel="stylesheet" href="utilLib/bootstrap.min.css" type="text/css" media="screen" />
<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="stylesheet" href="http://cdn.static.runoob.com/libs/bootstrap/3.3.7/css/bootstrap.min.css">  
	<script src="http://cdn.static.runoob.com/libs/jquery/2.1.1/jquery.min.js"></script>
	<script src="http://cdn.static.runoob.com/libs/bootstrap/3.3.7/js/bootstrap.min.js"></script>
</head>
<script  type="text/javascript">
	function haha()
	{window.location.href="Website.jsp";}
	function hehe()
	{document.getElementById("submits").submit();}
</script>
<style>
	td,th{padding-left:20px;}
</style>
<body onload="hehe();">

	<div class="table_con">
	<table>
    	<tr class="tb_title">
        <td style="margin-top:5px;">
        	<button type="button" class="btn btn-info">站点列表</button>
            <button type="button" class="btn btn-info" onClick="haha()">添加网站</button>
            <button type="button" class="btn btn-info">删除所有提交网站</button>
        </td></tr>
        <tr><td style="padding-left:40px;"><ul><li>站点列表</li></ul></td></tr>
    </table>
	</div>
    
	
    <div class="table_con">
    <form action="urlsubmit.spring" id="submits">
	<table>
	
     <tr class="tb_title" style="font-weight:bold;">
        	<td width="5%">ID</td>
        	<td width="10%">访问数量</td>
            <td width="25%">网址</td>
            <td width="15%">提交时间</td>
            <td width="15%">IP</td>
            <td width="25%">操作</td>
        </tr>
     <c:forEach items="${url}" var="u">   
        <tr>
        	<td width="5%">${u.id}</td>
        	<td width="10%">${u.number}</td>
        	<td width="25%">${u.url}</td>
            <td width="15%">2016-12-18</td>
            <td width="15%">${u.IP}</td>
            <td width="25%">
            	<input class="bj_btn" type="button" value="收录" />
                <input class="sj_btn" type="button" value="修改" />
                <input class="del_btn" type="button" value="删除" />
            </td>
        </tr>
       </c:forEach> 
        <tr >
        	<td colspan="5" style="margin-left:330px;">
                <a>第一页 </a>
                <a>上一页 </a>
                <a>下一页 </a>
                <a>最后一页 </a>
                <a>当前第1页 </a>
                <a>共1页</a>
                <a>共7个记录 </a>
             </td></tr>
    </table>
    </form>
    </div>
    </body>
</html>