<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>搜索引擎管理后台</title>
<link rel="stylesheet" href="css/index.css" type="text/css" media="screen"/>
<script type="text/javascript" src="js/jquery.min.js"></script>
<script type="text/javascript" src="js/tendina.min.js"></script>
<script type="text/javascript" src="js/common.js"></script>

</head>
<body>
    <!--顶部-->
    <div class="top">
            <div style="float: left">
            <span id="font-face-display"> 
            搜索引擎后台管理中心</span></div>
            <div id="ad_setting" class="ad_setting">
                <a class="ad_setting_a" href="#">findwords</a>
                <ul class="dropdown-menu-uu" style="display: none" id="ad_setting_ul">
                    <li class="ad_setting_ul_li"> <a href="javascript:;">
                    <i class="icon-cog glyph-icon"></i>设置</a> </li>
                    <li class="ad_setting_ul_li"><a href="javascript:;">
                    <i class="icon-signout glyph-icon"></i><span class="font-bold">退出</span> </a> </li>
                </ul>
                <img class="use_xl" src="images/right_menu.png" />
            </div>
    </div>
    <!--顶部结束-->
   
    <!--菜单-->
    <div class="left-menu">
    
    	
        <ul id="menu">
           <!--日常管理-->
           <li style="height:50px;margin-left:30px;"><span style="font-size:26px;">管理列表</span></li>
            <li class="menu-list">
               <a style="cursor:pointer" class="firsta">
               		<i  class="glyph-icon xlcd"></i>日常管理<s class="sz"></s>
                </a>
                <ul>
                    <li><a href="Homeformsselect" target="menuFrame">
                    	<i class="glyph-icon icon-chevron-right1"></i>首页表单添加</a></li>
                    <li><a href="URL_Submission.jsp" target="menuFrame">
                    	<i class="glyph-icon icon-chevron-right3"></i>网址提交</a></li>
                </ul>
            </li>
            <!--广告管理-->
            <li class="menu-list">
                <a style="cursor:pointer" class="firsta">
               		<i  class="glyph-icon xlcd"></i>广告管理<s class="sz"></s>
                </a>
                <ul>
                     <li><a href="Ad_management.jsp" target="menuFrame">
                     <i class="glyph-icon icon-chevron-right1"></i>广告操作</a></li>
                </ul>
            </li>
            
             <!--账户管理-->
            <li class="menu-list">
               <a style="cursor:pointer" class="firsta">
               		<i  class="glyph-icon xlcd"></i>账户管理<s class="sz"></s>
                </a>
                <ul>
                    <li><a href="Addadministrator.jsp" target="menuFrame">
                    	<i class="glyph-icon icon-chevron-right1"></i>管理员操作</a></li>
                    <li><a href="member.jsp" target="menuFrame">
                    	<i class="glyph-icon icon-chevron-right2"></i>会员操作</a></li>
                </ul>
            </li>
            
             <!--关键词管理-->
            <li class="menu-list">
               <a style="cursor:pointer" class="firsta">
               		<i  class="glyph-icon xlcd"></i>关键词管理<s class="sz"></s>
                </a>
                <ul>
                    <li><a href="Keywords.jsp" target="menuFrame">
                    	<i class="glyph-icon icon-chevron-right3"></i>关键词操作/官网直达</a></li>
                </ul>
            </li>
            
             <!--蜘蛛管理-->
            <li class="menu-list">
               <a style="cursor:pointer" class="firsta">
               		<i  class="glyph-icon xlcd"></i>蜘蛛管理<s class="sz"></s>
                </a>
               <ul>
                    <li><a href="Included.jsp" target="menuFrame">
                    	<i class="glyph-icon icon-chevron-right1"></i>收录管理</a></li>
                    <li><a href="URL_records.jsp" target="menuFrame">
                    	<i class="glyph-icon icon-chevron-right3"></i>网址记录</a></li>
                </ul>
            </li>          
        </ul>
    </div>
    
    <!--菜单右边的iframe页面-->
    <div id="right-content" class="right-content">
           <div class="content"></div>
		 	<div id="page_content">
                        <iframe id="menuFrame" name="menuFrame" src="Welcome.jsp" style="overflow:visible;"
                                scrolling="yes" frameborder="no" width="100%" height="85%"></iframe>
            </div>
    </div>
</body>
</html>
