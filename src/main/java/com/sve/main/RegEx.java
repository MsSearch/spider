package com.sve.main;

import java.io.IOException;
import java.util.Iterator;
import java.util.regex.Matcher;
import java.util.regex.Pattern;


import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import java.util.StringTokenizer;
public class RegEx {

	public static void main(String[] args) throws Exception 
	{
		// TODO Auto-generated method stub
		
		String robot="https://www.baidu.com/robots.txt";
		//1
		//http.+.net
		
		
		//第一
    	//String regEx ="/http.*net/";
		
		
    	Pattern pattern = Pattern.compile("http.*?.com");
    	Matcher matcher= pattern.matcher(robot);
    	
    	
    	
    	//Document doc =Jsoup.connect("")
    	
    	//第二
    	//String regEx2="/http(\S*).net/";
    	
    	//boolean rs = matcher.matches();
        //System.out.println(rs+":"+"结果"+":"+matcher.group());
    	
    	//重点1111111111111111111111
    	String group =null;
    	while(matcher.find())
    	{  
    		group=matcher.group();
    		System.out.println(group);
    		
    	}
    	
    	String robothtml=group+"/robots.txt";
    	Document doc = Jsoup.connect(robothtml).userAgent("Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.31 (KHTML, like Gecko) Chrome/26.0.1410.64 Safari/537.31").ignoreContentType(true).get();
    	//Element element = doc.body();
    	//Elements pre = element.select("pre"); 
    	RegEx re = new RegEx();
    	re.parse(doc.toString(), "ckj");
    	
//System.out.println(robothtml+"："+element.text());
    	
    	/*
    	Pattern pattern1 = Pattern.compile("\\s*.*\\*");
    	Pattern pattern2 = Pattern.compile(".*.*\\*");
    	//Pattern pattern1 = Pattern.compile("^ *.*\n$");
    	Matcher matcher1= pattern1.matcher(element.text().toString());
    	Matcher matcher2= pattern2.matcher(element.text().toString());
    	System.out.println("开始"+element.text().toString());
    	while(matcher1.find())
    	{
    		String e = matcher1.group(0);
    		System.out.println(e);
    	}
    	while(matcher2.find())
    	{
    		String e = matcher2.group(0);
    		System.out.println(e);
    	}
	
	*/
    	
    	
    //解析
    	
	}
	// 当使用String.matches方法调用时，"?i"表示忽略大小写 
	  private static final String PATTERNS_USERAGENT = "(?i)^User-agent:.*";
	  private static final String PATTERNS_DISALLOW = "(?i)Disallow:.*";
	  private static final String PATTERNS_ALLOW = "(?i)Allow:.*";

	  // "User-agent:"长度为11
	  private static final int PATTERNS_USERAGENT_LENGTH = 11;
	  private static final int PATTERNS_DISALLOW_LENGTH = 9;
	  private static final int PATTERNS_ALLOW_LENGTH = 6;
	public HostDirectives parse(String context,String myUserAgent)
	{
		System.out.println("真正开始了");
		HostDirectives directives = null;
		boolean inMatchingUserAgent = false;
		StringTokenizer st = new StringTokenizer(context,"\n\r");
		while (st.hasMoreTokens()) 
		{
			String line = st.nextToken();
			
			// #号之后的都是注释
		      int commentIndex = line.indexOf("#");
		      if (commentIndex > -1) 
		      {
		        line = line.substring(0, commentIndex);
		      }
		   // remove any html markup
		      line = line.replaceAll("<[^>]+>", "");    // "<[除了右括号的字符]+>"
		      line = line.trim();

		      if (line.length() == 0) {
		        continue;
		      }
		      
		      if (line.matches(PATTERNS_USERAGENT)) 
		      {   // User-agenet行的内容
		          String ua = line.substring(PATTERNS_USERAGENT_LENGTH).trim().toLowerCase();
		          // user-agent是否是针对当前爬虫的
		          if (ua.equals("*") || ua.contains(myUserAgent)) 
		          {
		            inMatchingUserAgent = true;
		          } 
		          else
		          {
		            inMatchingUserAgent = false;
		          }
		      }
		      else if (line.matches(PATTERNS_DISALLOW)) { // disallow行的内容
		          if (!inMatchingUserAgent) {
		            continue;
		          }
		          String path = line.substring(PATTERNS_DISALLOW_LENGTH).trim();
		          if (path.endsWith("*")) {
		              // 获取星号之前的path路径
		              path = path.substring(0, path.length() - 1);
		          }
		          path = path.trim();
		          if (path.length() > 0) {
		            if (directives == null) {
		              directives = new HostDirectives();
		            }
		            // 增加disallow规则
		            directives.addDisallow(path);
		          }
		        } else if (line.matches(PATTERNS_ALLOW)) {    // allow行的内容
		          if (!inMatchingUserAgent) {
		            continue;
		          }
		          String path = line.substring(PATTERNS_ALLOW_LENGTH).trim();
		          // 获取星号之前的Path路径
		          if (path.endsWith("*")) {
		            path = path.substring(0, path.length() - 1);
		          }
		          path = path.trim();
		          if (directives == null) {
		            directives = new HostDirectives();
		          }
		          System.out.println("看规则");
		          System.out.println(path);
		          // 增加allow规则
		          directives.addAllow(path);
		        }
			}	
		return directives;
		}
}
