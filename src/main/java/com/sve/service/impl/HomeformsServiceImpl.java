package com.sve.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.sve.dao.HomeformsMapper;
import com.sve.model.Homeforms;
import com.sve.service.IHomeformsService;

@Service("homeformsService")
public class HomeformsServiceImpl implements IHomeformsService {

	private HomeformsMapper homeformsMapper;
	
	public HomeformsMapper getHomeformsMapper() {
		return homeformsMapper;
	}
	@Autowired
	public void setHomeformsMapper(HomeformsMapper homeformsMapper) {
		this.homeformsMapper = homeformsMapper;
	}

	public List<Homeforms> sehomeforms() {
		// TODO Auto-generated method stub
		return homeformsMapper.sehomeforms();
	}
	public void inhomeforms(Homeforms homeforms) {
		// TODO Auto-generated method stub

		
		homeformsMapper.inhomeforms(homeforms);
	}
	public List<Homeforms> sehomeforms2(int id) {
		// TODO Auto-generated method stub
		return homeformsMapper.sehomeforms2(id);
	}
	
	public void uphomeforms(Homeforms homeforms)
	{
		// TODO Auto-generated method stub
		homeformsMapper.uphomeforms(homeforms);
	}
	public void dehomeforms(Homeforms homeforms) {
		// TODO Auto-generated method stub
		homeformsMapper.dehomeforms(homeforms);
	}

}
