package com.sve.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.sve.dao.PageMapper;
import com.sve.model.Page;
import com.sve.service.IPageService;

@Service("pageService")
public class PageServiceImpl implements IPageService {

	private PageMapper pagemapper;
	
	public PageMapper getPagemapper() {
		return pagemapper;
	}

	@Autowired
	public void setPagemapper(PageMapper pagemapper) {
		this.pagemapper = pagemapper;
	}

	public List<Page> LoadPage(String title,int num) {
		
		return pagemapper.query(title,num);
	}

}
