package com.sve.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.sve.dao.AdvertisingMapper;
import com.sve.model.Advertising;
import com.sve.service.IAdvertisingService;

@Service("addvertisingService")
public class AdvertisingServiceImpl implements IAdvertisingService {

	private AdvertisingMapper advertisingmapper;
	public AdvertisingMapper getAdvertisingmapper() {
		return advertisingmapper;
	}
	@Autowired
	public void setAdvertisingmapper(AdvertisingMapper advertisingmapper) {
		this.advertisingmapper = advertisingmapper;
	}
	public List<Advertising> LoadAdver() {
		return advertisingmapper.Advertising_query();
	}

}
