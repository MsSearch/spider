package com.sve.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.sve.dao.WaitMapper;
import com.sve.model.Wait;
import com.sve.service.IWaitService;

@Service("waitService")
public class WaitServiceImpl implements IWaitService 
{
	private WaitMapper waitmapper;
	public WaitMapper getWaitmapper() {
		return waitmapper;
	}
	@Autowired
	public void setWaitmapper(WaitMapper waitmapper) {
		this.waitmapper = waitmapper;
	}
	
	
	public List<Wait> wait(int waitid) {
		// TODO Auto-generated method stub
		return waitmapper.selectwait(waitid);
	}

}
