package com.sve.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.sve.dao.UrlsubmitMapper;
import com.sve.model.Urlsubmit;
import com.sve.service.IUrlService;

@Service("urlsubmitService")
public class UrlsubmitServiceImpl implements IUrlService {

	private UrlsubmitMapper urlsubmitmapper;

	public UrlsubmitMapper getUrlsubmitmapper() {
		return urlsubmitmapper;
	}
	@Autowired
	public void setUrlsubmitmapper(UrlsubmitMapper urlsubmitmapper) {
		this.urlsubmitmapper = urlsubmitmapper;
	}
	
	public List<Urlsubmit> LoadUrlsubmit() {
		// TODO Auto-generated method stub
		return urlsubmitmapper.Urlsubmit_query();
	}

}
