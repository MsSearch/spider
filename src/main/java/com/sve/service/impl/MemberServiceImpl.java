package com.sve.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.sve.dao.MemberMapper;
import com.sve.model.Member;
import com.sve.service.IMemberService;

@Service("memberService")
public class MemberServiceImpl implements IMemberService {

	private MemberMapper membermapper;
	public MemberMapper getMembermapper() {
		return membermapper;
	}
	@Autowired
	public void setMembermapper(MemberMapper membermapper) {
		this.membermapper = membermapper;
	}
	public List<Member> LoadMember() {
		// TODO Auto-generated method stub
		return membermapper.Member_query();
	}

}
