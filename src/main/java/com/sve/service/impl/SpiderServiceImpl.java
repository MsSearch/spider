package com.sve.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.sve.dao.SpiderMapper;
import com.sve.model.Spider;


@Service("spiderService")
public class SpiderServiceImpl implements SpiderMapper 
{
	private SpiderMapper spidermapper; 

	

	public SpiderMapper getSpidermapper() {
		return spidermapper;
	}


	@Autowired
	public void setSpidermapper(SpiderMapper spidermapper) {
		this.spidermapper = spidermapper;
	}



	public boolean add(Spider spider) 
	{
		return spidermapper.add(spider);
	}

}
