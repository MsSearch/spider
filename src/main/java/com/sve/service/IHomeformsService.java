package com.sve.service;

import java.util.List;

import com.sve.model.Homeforms;

public interface IHomeformsService 
{
	/*
	 * 加载首页表单数据
	 */
	List<Homeforms> sehomeforms();
	
	//Object insertforms(String hometitle,String homeurl,int homedisplay,int traffic);
	
	//插入表单数据
	void inhomeforms(Homeforms homeforms);
	
	/*
	 * 修改时通过id到数据库查询
	 */
	List<Homeforms> sehomeforms2(int id);
	
	//通过id修改数据
	void uphomeforms(Homeforms homeforms);
	
	//通过id删除数据
	void dehomeforms(Homeforms homeforms);
}
