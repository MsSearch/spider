package com.sve.service;

import java.util.List;

import com.sve.model.Wait;

public interface IWaitService 
{
	List<Wait> wait(int id);
}
