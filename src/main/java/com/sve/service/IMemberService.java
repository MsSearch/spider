package com.sve.service;

import java.util.List;

import com.sve.model.Member;

public interface IMemberService {
	
	List<Member> LoadMember();
}
