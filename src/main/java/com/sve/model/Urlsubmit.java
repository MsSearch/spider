package com.sve.model;

import java.util.Date;

public class Urlsubmit {
	private int id;
	private int number;
	private String url;
	private String createtime;
	private String IP;
	@Override
	public String toString() {
		return "URLsubmit [id=" + id + ", number=" + number + ", url=" + url + ", createtime=" + createtime + ", IP="
				+ IP + "]";
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public int getNumber() {
		return number;
	}
	public void setNumber(int number) {
		this.number = number;
	}
	public String getUrl() {
		return url;
	}
	public void setUrl(String url) {
		this.url = url;
	}
	public String getCreatetime() {
		return createtime;
	}
	public void setCreatetime(String createtime) {
		this.createtime = createtime;
	}
	public String getIP() {
		return IP;
	}
	public void setIP(String iP) {
		IP = iP;
	}
	

}
