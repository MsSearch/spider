package com.sve.model;

public class Advertising {
	private int id;
	private String title;
	private String photo_url;
	private String url;
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public String getPhoto_url() {
		return photo_url;
	}
	public void setPhoto_url(String photo_url) {
		this.photo_url = photo_url;
	}
	public String getUrl() {
		return url;
	}
	public void setUrl(String url) {
		this.url = url;
	}
	@Override
	public String toString() {
		return "Advertising [id=" + id + ", title=" + title + ", photo_url=" + photo_url + ", url=" + url + "]";
	}
	
	
}
