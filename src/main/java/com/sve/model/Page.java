package com.sve.model;

public class Page {

	private String title;
	private String httptext;
	private String url;
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public String getHttptext() {
		return httptext;
	}
	public void setHttptext(String httptext) {
		this.httptext = httptext;
	}
	public String getUrl() {
		return url;
	}
	public void setUrl(String url) {
		this.url = url;
	}
	@Override
	public String toString() {
		return "PageMapper [title=" + title + ", httptext=" + httptext + ", url=" + url + "]";
	}
}
