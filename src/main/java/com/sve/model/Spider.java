package com.sve.model;

public class Spider 
{
	private int id;
	public int getId() {
		return id;
	}
	
	public Spider()
	{
			
	}
	
	
	@Override
	public String toString() {
		return "Spider [id=" + id + ", title=" + title + ", httptext=" + httptext + ", url=" + url + "]";
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public String getHttptext() {
		return httptext;
	}
	public void setHttptext(String httptext) {
		this.httptext = httptext;
	}
	public String getUrl() {
		return url;
	}
	public void setUrl(String url) {
		this.url = url;
	}
	private String title;
	private String httptext;
	private String url;
}
