package com.sve.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import com.sve.model.Page;
import com.sve.service.IPageService;

@Controller
public class PageController {

	private IPageService pageservice;

	public IPageService getPageservice() {
		return pageservice;
	}
	@Autowired
	public void setPageservice(IPageService pageservice) {
		this.pageservice = pageservice;
	}
	
	@RequestMapping("/keywords")
	public String keyword(@RequestParam("keyname") String keyname,Model model,@RequestParam("pagepage") int num)
	{
			if(num!=0)
			{
				num=num-1;
			}
			int number =8*num;
			System.out.println(keyname+"lalalalalalalala");
			String name="%"+keyname+"%";
			
			List<Page> page=pageservice.LoadPage(name,number);
			model.addAttribute("context", page);
			model.addAttribute("text",keyname);
			return "Into";
	}
}
