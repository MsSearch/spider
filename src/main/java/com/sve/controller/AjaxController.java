package com.sve.controller;

import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.sve.model.Ajax;

@Controller
@RequestMapping("api")
public class AjaxController 
{
	@RequestMapping("keyname")
	public @ResponseBody List<Ajax> getkey(String name) 
	{
		System.out.println("name:"+name);
		List<Ajax> result = new ArrayList<Ajax>();
		Ajax ajax1 = new Ajax();
		ajax1.setName("ckj");
		Ajax ajax2 = new Ajax();
		ajax2.setName("ck1");
		Ajax ajax3 = new Ajax();
		ajax3.setName("ck2");
		result.add(ajax3);
		result.add(ajax2);
		result.add(ajax1);
		return result;
	}
	@RequestMapping("back")
	public String test()
	{
		return "WEB-INF/back/Backstage";
	}
}
