package com.sve.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import com.sve.model.Urlsubmit;
import com.sve.service.impl.UrlsubmitServiceImpl;

@Controller
public class UrlsubmitController {

	private UrlsubmitServiceImpl urlsubmitservice;

	public UrlsubmitServiceImpl getUrlsubmitservice() {
		return urlsubmitservice;
	}
	@Autowired
	public void setUrlsubmitservice(UrlsubmitServiceImpl urlsubmitservice) {
		this.urlsubmitservice = urlsubmitservice;
	}
	
	@RequestMapping("/urlsubmit")
	public String showurlsubmit(Model model)
	{
		List<Urlsubmit> url=urlsubmitservice.LoadUrlsubmit();
		model.addAttribute("url",url);
		System.out.println("====>"+url.get(0).getCreatetime());
		return "URL_Submission";
	}
}
