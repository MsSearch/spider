package com.sve.controller;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.ibatis.annotations.Param;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.fasterxml.jackson.databind.util.JSONPObject;
import com.sve.model.Homeforms;
import com.sve.service.IHomeformsService;

@Controller
public class HomeFormsController 
{
	private IHomeformsService homeformsService;
	public IHomeformsService getHomeformsService() {
		return homeformsService;
	}
	@Autowired
	public void setHomeformsService(IHomeformsService homeformsService) {
		this.homeformsService = homeformsService;
	}
	
	//查询
	@RequestMapping("Homeformsselect")
	public String getforms(Model model)
	{
		System.out.println("进来");
		List<Homeforms> home = homeformsService.sehomeforms();
		model.addAttribute("forms", home);
		return "Home_Add";
	}
	//增加
	@RequestMapping("Homeformsinsert")
	@ResponseBody
	public String insert(String hometitle,String homeurl,String homedisplay,int traffic)
	{
		System.out.println("得到的数据"+hometitle+homeurl+homedisplay+traffic);
		Homeforms h = new Homeforms();
		h.setHometitle(hometitle);
		h.setHomeurl(homeurl);
		h.setHomedisplay(homedisplay);
		h.setTraffic(traffic);
		homeformsService.inhomeforms(h);
		System.out.println("插入成功");
		return "Homeformsselect";
	}
	
	//通过id查询数据
	@RequestMapping("Homeformsselecttwo")
	@ResponseBody
	public List<Homeforms> select(int id)
	{
		List<Homeforms> list = new ArrayList<Homeforms>();
		list = homeformsService.sehomeforms2(id);
		return list;
	}

	
	//通过id修改数据
	@RequestMapping("updatehomeforms")
	public void updateforms(@Param("upid")int id,@Param("uptitle")String homeformstitle,@Param("upurl")String homeformsurl,@Param("uptraffic")int traffic,@Param("updisplay")String homeformsdisplay)
	{
		Homeforms forms = new Homeforms();
		forms.setId(id);
		forms.setHometitle(homeformstitle);
		forms.setHomeurl(homeformsurl);
		forms.setHomedisplay(homeformsdisplay);
		forms.setTraffic(traffic);
		homeformsService.uphomeforms(forms);
	}
	
	//通过id删除数据
	@RequestMapping("detelehomeforms")
	@ResponseBody
	public void deteleforms(int id)
	{
		System.out.println("进来!!!!!!!!!!");
		System.out.println(id);
		Homeforms forms = new Homeforms();
		forms.setId(id);
		homeformsService.dehomeforms(forms);
	}
}
