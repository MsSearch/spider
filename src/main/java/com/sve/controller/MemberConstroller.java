package com.sve.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import com.sve.model.Member;
import com.sve.service.IMemberService;

@Controller
public class MemberConstroller {

	private IMemberService memberservice;

	public IMemberService getMemberservice() {
		return memberservice;
	}
	@Autowired
	public void setMemberservice(IMemberService memberservice) {
		this.memberservice = memberservice;
	}
	@RequestMapping("/member")
	public String showmember(Model model)
	{
		List<Member> member=memberservice.LoadMember();
		model.addAttribute("member",member);
		return "member";
	}
	
	
}
