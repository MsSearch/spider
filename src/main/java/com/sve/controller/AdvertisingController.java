package com.sve.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import com.sve.model.Advertising;
import com.sve.service.IAdvertisingService;

@Controller
public class AdvertisingController {
	
	private IAdvertisingService adverservice;

	public IAdvertisingService getAdverservice() {
		return adverservice;
	}
	@Autowired
	public void setAdverservice(IAdvertisingService adverservice) {
		this.adverservice = adverservice;
	}
	
	@RequestMapping("/advertising")
	public String showadvertising(Model model)
	{
		List<Advertising> adver=adverservice.LoadAdver();
		model.addAttribute("adver", adver);
		return "Ad_management";
	}
	
}
