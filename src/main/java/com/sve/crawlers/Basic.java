package com.sve.crawlers;

import cn.wanghaomiao.seimi.annotation.Crawler;
import cn.wanghaomiao.seimi.def.BaseSeimiCrawler;
import cn.wanghaomiao.seimi.struct.Response;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.StringTokenizer;
import java.util.regex.*;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.ui.Model;

import com.sve.dao.MybatisSpider;
import com.sve.dao.SpiderMapper;
import com.sve.main.HostDirectives;
import com.sve.model.Spider;
import com.sve.model.Wait;
import com.sve.service.ISpiderService;
import com.sve.service.IWaitService;


@Crawler(name="basic")
public class Basic extends BaseSeimiCrawler 
{	
	private IWaitService waitserver;
	public String[] startUrls()
    {
        //两个是测试去重的
		return new String[]{"http://openjdk.java.net/"};
        //return new String[]{"http://blog.csdn.net/likika2012/article/details/11483167"};
    }

    public void start(Response response) 
    {
        //JXDocument doc = response.document();
        System.out.println("开始爬取");
    	try {
    		//Xpath的
            //List<Object> urls = doc.sel("//a[@class='titlelnk']/@href");
            //logger.info("{}", urls.size());
            
    		//robots.txt解析  调用方法
    		//String test = response.toString();
//            Basic basic = new Basic();
//            basic.parse(test, "ckj");
            
            
            
            //正则解析变为robots.txt格式
        	String robot = response.getUrl().toString();
        	System.out.println(robot);
        	
        	//正.net
//        	String regEx ="/http.*.net/";
//        	Pattern pattern = Pattern.compile(regEx);
//        	Matcher matcher= pattern.matcher(robot);       	
//        	boolean rs = matcher.matches();
//            System.out.println(rs+":"+"结果"+":"+matcher.group());
        	//结束
        	
        	
        	//jsoup解析
            String html = response.toString();
            Document doc = Jsoup.parse(html);
            Element body = doc.body();
            Elements es =body.select("p");
            Elements t =body.select("title");
            if(es.text()==null)
            {
            	es =body.select("div");
            }
           
            //标题
            String title = t.text();
            //网页连接
            String url = response.getUrl();
            
            //测试变量
            int num=0;
            //每一个标签的文本内容
            String nexttext = null;
            //最终判读的正文摘要
            String lasttext = null;            

            List<String> list=new ArrayList<String>();
            for(Iterator it  = es.iterator();it.hasNext();)
            { 
            	
            	Element e =(Element)it.next();
            	
            	list.add(e.text());
            	if(num!=0)
            	{
            		if(list.get(num-1).length()<=list.get(num).length())
        			{
            			if(num==1 && list.get(num-1).length()<=list.get(num).length())
            			{
            				lasttext=list.get(num);
            			}
            			else if(num==1 && list.get(num-1).length()>=list.get(num).length())
            			{
            				lasttext=list.get(num-1);
            			}
            			else
            			{
            				if(lasttext.length()<=list.get(num).length())
            				{
            					lasttext=list.get(num);
            				}
            			}
        			}	
            		else if(num==1 &&list.get(num-1).length()>list.get(num).length())
            		{
            			lasttext=list.get(num-1);
            		}
            	}
            	
            	num=num+1;
            }
            //查看最后的文本摘要内容
            System.out.println(lasttext+" "+"hehe");
            
            if(lasttext.length()>100)
            {
            	lasttext =lasttext.substring(0, 105)+"...";
            }
              //存入数据表
              Spider spider = new Spider();
              spider.setId(14);
              spider.setTitle(title);
              spider.setUrl(url);
              spider.setHttptext(lasttext);
              MybatisSpider dao = new MybatisSpider();
              dao.insert(spider);
            
              
//              List<Wait> wait = waitserver.wait(1);
//              
//              Model model = null;
//              model.addAttribute(wait);
//              System.out.println(wait.get(0));
              
              
          //跳转爬取的方法
            //push(new Request("https://www.liepin.com/#sfrom=click-pc_homepage-front_navigation-index_new/", "getTitle"));
         
            
            /*
            for (Object s:urls){
            	System.out.println("谔谔谔谔谔谔谔谔");
                push(new Request(s.toString(),"getTitle"));
            }
            */
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
//    private int number=1;
//    public void getTitle(Response response)
//    {
//        JXDocument doc = response.document();
//        String html = response.toString();
//        Document doc1 = Jsoup.parse(html);
//        try {
//        	Element body = doc1.body();
//            Elements es =body.select("a");
//            for(Iterator it  =es.iterator();it.hasNext();)
//            {
//            	Element e =(Element)it.next();
//            	System.out.println(e.text()+" "+e.attr("href"));
//            }
//        	System.out.println("内容：dd"+":"+response.getUrl().toString());
//        	List<Object> urls = doc.sel("//a[@class='search-link']/@innerHTML");
//        	System.out.println("内容："+urls);
//        	push(new Request("http://www.cnblogs.com/", "getTitle"));
//        	List<Object> urls1 = doc.sel("//a[@class='post_item_summary']/@innerHTML");
//        	number=1+number;
//        	System.out.println("内容11："+urls1+number);
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
//    }
// // 当使用String.matches方法调用时，"?i"表示忽略大小写 
// 	  private static final String PATTERNS_USERAGENT = "(?i)^User-agent:.*";
// 	  private static final String PATTERNS_DISALLOW = "(?i)Disallow:.*";
// 	  private static final String PATTERNS_ALLOW = "(?i)Allow:.*";
//
// 	  // "User-agent:"长度为11
// 	  private static final int PATTERNS_USERAGENT_LENGTH = 11;
// 	  private static final int PATTERNS_DISALLOW_LENGTH = 9;
// 	  private static final int PATTERNS_ALLOW_LENGTH = 6;
//    public HostDirectives parse(String context,String myUserAgent)
//	{
//		System.out.println("真正开始了");
//		HostDirectives directives = null;
//		boolean inMatchingUserAgent = false;
//		StringTokenizer st = new StringTokenizer(context,"\n\r");
//		while (st.hasMoreTokens()) 
//		{
//			String line = st.nextToken();
//			// #号之后的都是注释
//		      int commentIndex = line.indexOf("#");
//		      if (commentIndex > -1) 
//		      {
//		        line = line.substring(0, commentIndex);
//		      }
//		   // remove any html markup
//		      line = line.replaceAll("<[^>]+>", "");    // "<[除了右括号的字符]+>"
//		      line = line.trim();
//
//		      if (line.length() == 0) 
//		      {
//		        continue;
//		      }
//		      
//		      if (line.matches(PATTERNS_USERAGENT)) 
//		      {   // User-agenet行的内容
//		          String ua = line.substring(PATTERNS_USERAGENT_LENGTH).trim().toLowerCase();
//		          System.out.println(ua);
//		          // user-agent是否是针对当前爬虫的
//		          if (ua.equals("*") || ua.contains(myUserAgent)) 
//		          {
//		            inMatchingUserAgent = true;
//		          } 
//		          else
//		          {
//		            inMatchingUserAgent = false;
//		          }
//		      }
//		      else if (line.matches(PATTERNS_DISALLOW)) 
//		      { // disallow行的内容
//		          if (!inMatchingUserAgent) {
//		            continue;
//		          }
//		          String path = line.substring(PATTERNS_DISALLOW_LENGTH).trim();
//		          //输出Disallow
//		          
//		          if (path.endsWith("*")) {
//		              // 获取星号之前的path路径
//		              path = path.substring(0, path.length() - 1);
//		          }
//		          path = path.trim();
//		          if (path.length() > 0) 
//		          {
//		            if (directives == null) {
//		              directives = new HostDirectives();
//		            }
//		            // 增加disallow规则
//		            directives.addDisallow(path);
//		          }
//		        } else if (line.matches(PATTERNS_ALLOW)) {    // allow行的内容
//		          if (!inMatchingUserAgent) {
//		            continue;
//		          }
//		          String path = line.substring(PATTERNS_ALLOW_LENGTH).trim();
//		          System.out.println("path:"+path);
//		          // 获取星号之前的Path路径
//		          if (path.endsWith("*")) {
//		            path = path.substring(0, path.length() - 1);
//		          }
//		          path = path.trim();
//		          if (directives == null) {
//		            directives = new HostDirectives();
//		          }
//		          System.out.println("看规则");
//		          System.out.println(path);
//		          // 增加allow规则
//		          directives.addAllow(path);
//		        }
//			}	
//		return directives;
//		}
}
