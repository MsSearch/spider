package com.sve.crawlers;

import cn.wanghaomiao.seimi.annotation.Crawler;
import cn.wanghaomiao.seimi.def.BaseSeimiCrawler;
import cn.wanghaomiao.seimi.struct.Response;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.StringTokenizer;
import java.util.regex.*;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import com.sve.dao.MybatisSpider;
import com.sve.dao.SpiderMapper;
import com.sve.main.HostDirectives;
import com.sve.model.Spider;
import com.sve.service.ISpiderService;




@Crawler(name="basicc")
public class Basic2 extends BaseSeimiCrawler 
{	

	public String[] startUrls()
    {
        //两个是测试去重的
		//return new String[]{"http://blog.csdn.net/robots.txt"};
        return new String[]{"https://www.jd.com/robots.txt"};
    }

    public void start(Response response) 
    {
        //JXDocument doc = response.document();
        System.out.println(response.toString());
    	try {
            //List<Object> urls = doc.sel("//a[@class='titlelnk']/@href");
            //logger.info("{}", urls.size());
            String test = response.getContent();
            //System.out.println(test);
            Basic2 basic = new Basic2();
            basic.parse(test, "ckj");
            
            
            
            HostDirectives ht = new HostDirectives();
            System.out.println("最后的效果："+ht.allows("ds"));
            //测试
            System.out.println("最后的效果："+ht.getAllows());
            System.out.println("最后的效果："+ht.getDisallows());
            
            
            ///////////////////////////////////////////////////////////////////////////////
            //////////////////////////////////////////////////////////////////////////////
        	//爬取规则
        	String robot = response.getUrl().toString();
        	System.out.println(robot);
        	
        	//第一
        	String regEx ="/http.*.net/";
        	Pattern pattern = Pattern.compile(regEx);
        	Matcher matcher= pattern.matcher(robot);
        	
        	
        	
        	
        	
        	//第二
        	//String regEx2="/http(\S*).net/";
        	
//        	boolean rs = matcher.matches();
//            System.out.println(rs+":"+"结果"+":"+matcher.group());
        	
        	//结束
        	
//        	spidermidel midel = new spidermidel();
//        	spiderDao dao = new spiderDao();
            System.out.println("开始");
            String html = response.toString();
            Document doc = Jsoup.parse(html);
            Element body = doc.body();
            Elements es =body.select("p");

            
            
            int num=0;
            String nexttext = null;
            String lasttext = null;            

            
            List<String> list=new ArrayList<String>();
            for(Iterator it  = es.iterator();it.hasNext();)
            { 
            	
            	Element e =(Element)it.next();
            	
            	list.add(e.text());
            	if(num!=0)
            	{
            		if(list.get(num-1).length()<=list.get(num).length())
        			{
            			if(num==1 && list.get(num-1).length()<=list.get(num).length())
            			{
            				lasttext=list.get(num);
            			}
            			else if(num==1 && list.get(num-1).length()>=list.get(num).length())
            			{
            				lasttext=list.get(num-1);
            			}
            			else
            			{
            				if(lasttext.length()<=list.get(num).length())
            				{
            					lasttext=list.get(num);
            				}
            			}
        			}	
            		else if(num==1 &&list.get(num-1).length()>list.get(num).length())
            		{
            			lasttext=list.get(num-1);
            		}
            	}
            	
            	num=num+1;
            }
            System.out.println(lasttext+" "+"hehe");
            //push(new Request("https://www.liepin.com/#sfrom=click-pc_homepage-front_navigation-index_new/", "getTitle"));
            
            String url =response.getUrl().toString();
            String title ="测试biaoti";
            int id = 2;
            
            
            
            
            
            
//            //第1次数据测试
//            Spider spider = new Spider();
//            spider.setId(id);
//            spider.setTitle(title);
//            spider.setUrl(url);
//            spider.setHttptext("q1234656ufix");
//            
//            MybatisSpider dao = new MybatisSpider();
//            dao.insert(spider);
//            
            /*
            for (Object s:urls){
            	System.out.println("谔谔谔谔谔谔谔谔");
                push(new Request(s.toString(),"getTitle"));
            }
            */
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
//    private int number=1;
//    public void getTitle(Response response)
//    {
//        JXDocument doc = response.document();
//        String html = response.toString();
//        Document doc1 = Jsoup.parse(html);
//        try {
//        	Element body = doc1.body();
//            Elements es =body.select("a");
//            for(Iterator it  =es.iterator();it.hasNext();)
//            {
//            	Element e =(Element)it.next();
//            	System.out.println(e.text()+" "+e.attr("href"));
//            }
//        	System.out.println("内容：dd"+":"+response.getUrl().toString());
//        	List<Object> urls = doc.sel("//a[@class='search-link']/@innerHTML");
//        	System.out.println("内容："+urls);
//        	push(new Request("http://www.cnblogs.com/", "getTitle"));
//        	List<Object> urls1 = doc.sel("//a[@class='post_item_summary']/@innerHTML");
//        	number=1+number;
//        	System.out.println("内容11："+urls1+number);
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
//    }
 // 当使用String.matches方法调用时，"?i"表示忽略大小写 
 	  private static final String PATTERNS_USERAGENT = "(?i)^User-agent:.*";
 	  private static final String PATTERNS_DISALLOW = "(?i)Disallow:.*";
 	  private static final String PATTERNS_ALLOW = "(?i)Allow:.*";

 	  // "User-agent:"长度为11
 	  private static final int PATTERNS_USERAGENT_LENGTH = 11;
 	  private static final int PATTERNS_DISALLOW_LENGTH = 9;
 	  private static final int PATTERNS_ALLOW_LENGTH = 6;
    public HostDirectives parse(String context,String myUserAgent)
	{
		System.out.println("真正开始了");
		HostDirectives directives = null;
		boolean inMatchingUserAgent = false;
		StringTokenizer st = new StringTokenizer(context,"\n\r");
		while (st.hasMoreTokens()) 
		{
			String line = st.nextToken();
			// #号之后的都是注释
		      int commentIndex = line.indexOf("#");
		      if (commentIndex > -1) 
		      {
		        line = line.substring(0, commentIndex);
		      }
		   // remove any html markup
		      line = line.replaceAll("<[^>]+>", "");    // "<[除了右括号的字符]+>"
		      line = line.trim();//把空格和起始位置都清空
		      
		      if (line.length() == 0) 
		      {
		    	System.out.println("有一个为0");  
		        continue;
		      }
		      
		      if (line.matches(PATTERNS_USERAGENT)) 
		      {   
		    	  System.out.println("User-agenet的方法");
		    	  // User-agenet行的内容
		          String ua = line.substring(PATTERNS_USERAGENT_LENGTH).trim().toLowerCase();
		          System.out.println("User-agenet行的内容:"+ua);
		          // user-agent是否是针对当前爬虫的
		          if (ua.equals("*") || ua.contains(myUserAgent)) 
		          {
		        	System.out.println("不为空");
		            inMatchingUserAgent = true;
		          } 
		          else
		          {
		        	  System.out.println("为空");
		            inMatchingUserAgent = false;
		          }
		      }
		      else if (line.matches(PATTERNS_DISALLOW)) 
		      { // disallow行的内容
		          if (!inMatchingUserAgent) {
		        	
		            continue;
		          }
		          String path = line.substring(PATTERNS_DISALLOW_LENGTH).trim();
		          //输出Disallow
		          System.out.println("disallow行的内容：："+path);
		          if (path.endsWith("*")) {
		              // 获取星号之前的path路径
		              path = path.substring(0, path.length() - 1);
		          }
		          path = path.trim();
		          if (path.length() > 0) 
		          {
		            if (directives == null) {
		              directives = new HostDirectives();
		            }
		            // 增加disallow规则
		            directives.addDisallow(path);
		          }
		        } else if (line.matches(PATTERNS_ALLOW)) {    // allow行的内容
		          if (!inMatchingUserAgent) {
		            continue;
		          }
		          String path = line.substring(PATTERNS_ALLOW_LENGTH).trim();
		          System.out.println("行的行path:"+path);
		          // 获取星号之前的Path路径
		          if (path.endsWith("*")) {
		            path = path.substring(0, path.length() - 1);
		          }
		          path = path.trim();
		          if (directives == null) {
		            directives = new HostDirectives();
		          }
		          System.out.println("看规则");
		          System.out.println(path);
		          // 增加allow规则
		          directives.addAllow(path);
		        }
			}
		return directives;
		}
}
