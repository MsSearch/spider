package com.sve.dao;

import java.util.List;

import org.apache.ibatis.annotations.Param;

import com.sve.model.Page;

public interface PageMapper {

	public List<Page> query(@Param(value="title") String title,@Param(value="num") int num);	
}
