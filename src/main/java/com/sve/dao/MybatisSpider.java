package com.sve.dao;

import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.apache.ibatis.session.SqlSessionFactoryBuilder;

import com.sve.model.Spider;

public class MybatisSpider 
{
	SqlSessionFactory sessionFactory=new SqlSessionFactoryBuilder().build(MybatisSpider.class.getResourceAsStream("/mybatis-config1.xml"));
    SqlSession session=sessionFactory.openSession();
    
    public void insert(Spider spider){
   	 Spider s=new Spider();
   	 s.setId(spider.getId());
   	 s.setHttptext(spider.getHttptext());
   	 s.setTitle(spider.getTitle());
   	 s.setUrl(spider.getUrl());
   	 session.insert("add",s);
   	 session.commit(); 
   	
 
    }
}
