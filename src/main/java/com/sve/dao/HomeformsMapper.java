package com.sve.dao;

import java.util.List;

import org.apache.ibatis.annotations.Param;

import com.sve.model.Homeforms;

public interface HomeformsMapper 
{
	/*
	 * 查询首页表单的数据
	 */
	List<Homeforms> sehomeforms();
	
	/*
	 * 插入数据
	 */
	void inhomeforms(Homeforms homeforms);
	
	/*
	 * 通过id查询数据
	 */
	List<Homeforms> sehomeforms2(@Param("id")int id);
	
	//通过id修改数据
	void uphomeforms(Homeforms homeforms);
	
	
	//通过id删除数据
	void dehomeforms(Homeforms homeforms);
}
