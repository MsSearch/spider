package com.sve.dao;

import java.util.List;

import com.sve.model.Wait;

public interface WaitMapper 
{
	List<Wait> selectwait(int waitid);
}
